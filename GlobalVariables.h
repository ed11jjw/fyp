//These variables need to be used by ODE as well as other classes
static int num=0; // number of objects in simulation
static int nextobj=0; // next object to recycle if num==NUM
static dWorldID world; //Id of the world used by ODE
static dSpaceID space; //Space Id used by ODE
static MyObject obj[NUM]; //Array of the objects in the simulation
static dJointGroupID contactgroup; //Id to the array of contact objects used by ODE
static dJointGroupID jointgroup;
static float delta_t = 0.02;
//Store all of the musle geom ID's so that they can be ignored from collision detection
vector<dGeomID> MuscleGeoms;