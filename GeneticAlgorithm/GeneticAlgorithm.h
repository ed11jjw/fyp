using namespace std;
#ifndef GENETICALG_H
#define GENETICALG_H
#include <iostream>
#include <vector>
#include <math.h>
#include <ode/ode.h>
#include <algorithm>

#define GAPopulationSize 100
//How many of the fittest creatures to keep 
//for the next generation
#define GASelectionSize 10
#define GAMutationRate 0.2

class GeneticAlgorithm{
    public:
    vector<Creature*> Creatures;
	Creature *BaseCreature;
	int CurrentCreature, CurrentGeneration, PercentCount;
	bool Display;
	float FitnessSum;
	GeneticAlgorithm(Creature*, bool);
	Creature* NextCreature(Creature*);
	Creature* MutateCreature(Creature*);
	Creature* RandomCreature(Creature*);
	float MutateParameter(float);
	void NextGeneration();
	void LogGeneration();
	vector<Creature*> CombineCreaturesRandom(Creature*, Creature*);
	Creature* TruncateSelection();
	Creature* RouletteWheelSelection();
	bool CreaturesTheSame(Creature*, Creature*);
};
#endif
