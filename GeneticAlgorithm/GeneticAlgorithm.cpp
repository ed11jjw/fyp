#include <stdio.h>
#include <stdlib.h>
#include <string>  
#include <sstream>
#include <fstream>  
#include <time.h>
#include "GeneticAlgorithm.h"

GeneticAlgorithm::GeneticAlgorithm(Creature *baseCreature, bool display){
	BaseCreature = baseCreature;
	CurrentCreature = 0;
	CurrentGeneration = 0;
	Creatures.push_back(baseCreature);
	Display = display;
	PercentCount = 0;
	for(int i=1; i < GAPopulationSize; i++){ //Create initial population of random creatures
		Creature *nextCreature = RandomCreature(baseCreature);
		Creatures.push_back(nextCreature);
	}
	if(!Display)
		printf("Running Generation 0 |");
}

Creature* GeneticAlgorithm::NextCreature(Creature *simCreature){
	simCreature->RemoveFromSim();//Remove input creature from simulation
	if(Display){
		printf("%f\n", simCreature->LastDistanceFromOrigin);
	}
	
	if(CurrentCreature == GAPopulationSize - 1) //If last creature in gen move to next gen
		NextGeneration();
	else //Else move to next creature in gen
		CurrentCreature++;
	
	Creatures[CurrentCreature]->AddToSim(0); //Add creature to simulation
	if(Display){
		printf("Running creature %d ", CurrentCreature);
	}
	else{
		int currentPercent = CurrentCreature / ((GAPopulationSize - 1) / 20);
		if(currentPercent > PercentCount){
			printf("-");
			PercentCount = currentPercent;
		}
	}
	return Creatures[CurrentCreature];
}

Creature* GeneticAlgorithm::MutateCreature(Creature *simCreature){
	Creature *newCreature = simCreature->Copy();
	for (int i=0; i<simCreature->Muscles.size(); i++){ 
		// Mutate input creatures phase, amplitude and speed parameters for each muscle
		float p = MutateParameter(newCreature->Muscles[i]->Phase);
		float a = MutateParameter(newCreature->Muscles[i]->Amplitude);
		float s = MutateParameter(newCreature->Muscles[i]->Speed);
		newCreature->Muscles[i]->SetParams(p, a, s);
	}
	return newCreature;
}

float GeneticAlgorithm::MutateParameter(float val){ 
	return val + 
		((dRandReal() - 0.5) * 2 * //Random value between -1 and +1
		GAMutationRate * val); //Scale so that a mutation rate 
		//of 0.1 gives a max change of 10% for example
}

Creature* GeneticAlgorithm::RandomCreature(Creature *simCreature){
	Creature *newCreature = simCreature->Copy();
	for (int i=0; i<newCreature->Muscles.size(); i++){
		//Create random creature with random parameters for phase, amplitude and
		//speed within the given constraints for each muscle
		float p = dRandReal() * 360;
		float a = dRandReal() * AmplitudeLimit;
		float s = dRandReal() * SpeedLimit;
		newCreature->Muscles[i]->SetParams(p, a, s);
	}
	return newCreature;
}

struct CreatureBetter
{	//Compares two creature's distances, so that the sort function can order them
    bool operator()( const Creature* lc, const Creature* rc ) const {
    	return lc->LastDistanceFromOrigin > rc->LastDistanceFromOrigin;
    }
};

void GeneticAlgorithm::NextGeneration(){
	//Stop ODE non-determistic results
	dRandSetSeed (time(0));
	float bestDistance = 0.0;
	FitnessSum = 0.0;
	
	//Get the fitness sum of current generation
	for(int i=0; i<GAPopulationSize; i++)
		FitnessSum += Creatures[i]->LastDistanceFromOrigin;
	
	//Sort selected creatures
	sort(Creatures.begin(), Creatures.end(), CreatureBetter());
	
	if(Display){
		for(int i = 0; i < GASelectionSize; i++){
			printf("Creature %d scored %f\n", i, Creatures[i]->LastDistanceFromOrigin);
			
		}
		printf("\n");
	}
	else{
		printf("|\n");
	}
	LogGeneration();
	vector<Creature*> newCreatures;
	for(int i = 0; i < GASelectionSize; i++){
		newCreatures.push_back(Creatures[i]);
	}
	CurrentCreature = 0;
	vector<Creature*> crossoverCreatures;
	while(newCreatures.size() < GAPopulationSize){
		Creature* parent1 = RouletteWheelSelection();
		Creature* parent2 = RouletteWheelSelection();
		
		vector<Creature*> Offspring;
		//If creatures aren't the same, create two offspring by uniform crossover
		if(!CreaturesTheSame(parent1, parent2)){
			Offspring = CombineCreaturesRandom(parent1, parent2);
		}
		else{//If two creatures are clones create two new random creatures
			//(random offspring generation)
			Offspring.push_back(RandomCreature(parent1));
			Offspring.push_back(RandomCreature(parent2));
		}
		newCreatures.push_back(MutateCreature(Offspring[0]));
		newCreatures.push_back(MutateCreature(Offspring[1]));
		if(crossoverCreatures.size() >= GAPopulationSize)
			break;
	}
	
	Creatures = newCreatures;
	PercentCount = 0;
	CurrentGeneration++;
	printf("Running Generation %d\n",CurrentGeneration);
	if(!Display)
		printf("|");
}

Creature* GeneticAlgorithm::TruncateSelection(){
	int i = rand() %(GASelectionSize-1);
	
	return Creatures[i];
}

Creature* GeneticAlgorithm::RouletteWheelSelection(){
	//Roulette wheel selection, choose a random value between {0,1}
	//multiply it by the fitness value, get the weight of each creature 
	//by their fitness and minus amount from value, once value falls 
	//below 0, select that current creature, add it to the next generation,
	//remove from current generation and their fitness from the fitness sum,
	//repeat process until selection size is full.
	//for(int i=0; i<GASelectionSize; i++){
	float value = dRandReal()*FitnessSum;
	int i = 0;
	while(value>0){
		value -= Creatures[i]->LastDistanceFromOrigin;
		i++;
	}
	return Creatures[i-1];
}

vector<Creature*> GeneticAlgorithm::CombineCreaturesRandom(Creature *creature1, Creature *creature2){
	//If value is greater than or equal to 0.5, creature 1 pass parameter to offspring 1, creature
	//2 pass parameter to offspring 2. Else creature 1 pass parameter to offspring 2, creature 2
	//pass parameter to creature 1.
	Creature *newCreature1 = creature1->Copy();
	Creature *newCreature2 = creature2->Copy();
	vector<Creature*> Offspring;
	for (int i=0; i<newCreature1->Muscles.size(); i++){
		float p1, p2, a1, a2, s1, s2;
		if(dRandReal() >= 0.5){
			p1 = creature1->Muscles[i]->Phase; 
			p2 = creature2->Muscles[i]->Phase; 
		}
		else{
			p1 = creature2->Muscles[i]->Phase;
			p2 = creature1->Muscles[i]->Phase;
		}
		
		if(dRandReal() >= 0.5){
			a1 = creature1->Muscles[i]->Amplitude;
			a2 = creature2->Muscles[i]->Amplitude;
		}
		else{
			a1 = creature2->Muscles[i]->Amplitude;
			a2 = creature1->Muscles[i]->Amplitude;
		}
		
		if(dRandReal() >= 0.5){
			s1 = creature1->Muscles[i]->Speed;
			s2 = creature2->Muscles[i]->Speed;
		}
		else{
			s1 = creature2->Muscles[i]->Speed;
			s2 = creature1->Muscles[i]->Speed;
		}
			
		newCreature1->Muscles[i]->SetParams(p1, a1, s1);
		newCreature2->Muscles[i]->SetParams(p2, a2, s2);
	}
	Offspring.push_back(newCreature1);
	Offspring.push_back(newCreature2);
	
	return Offspring;
}

bool GeneticAlgorithm::CreaturesTheSame(Creature *creature1, Creature *creature2){
	//If both creature has all the same muscle parameters they are the same, create and
	//if statement tha checks if any parameter differs.
	for(int i=0; i<creature1->Muscles.size(); i++){
		if((creature1->Muscles[i]->Phase)!=(creature2->Muscles[i]->Phase)){
			return false;
		}
		else if((creature1->Muscles[i]->Amplitude)!=(creature2->Muscles[i]->Amplitude)){
			return false;
		}
		else if((creature1->Muscles[i]->Speed)!=(creature2->Muscles[i]->Speed)){
			return false;
		}
	}
	return true;
}

void GeneticAlgorithm::LogGeneration(){
	ofstream logFile;
	float avgDist = 0;
	for(int i = 0; i<Creatures.size();i++) //Get average distance put in log file
		avgDist+= Creatures[i]->LastDistanceFromOrigin;
	avgDist /= Creatures.size();
	
	logFile.open ("Log.csv", std::ios_base::app);
	//Display average and best distance
	logFile << CurrentGeneration << ',' << 
		avgDist << ',' << 
		Creatures[0]->LastDistanceFromOrigin <<  ',' <<
		Creatures[Creatures.size()-1]->LastDistanceFromOrigin << "\n";
	logFile.close();
	printf("Best: %f, Avg: %f, Worst: %f\n", Creatures[0]->LastDistanceFromOrigin, 
		avgDist, Creatures[Creatures.size()-1]->LastDistanceFromOrigin);
}