using namespace std;
#ifndef BONE_H
#define BONE_H
#include <iostream>
#include <vector>
#include <math.h>
#include <ode/ode.h>
#include "PID.cpp"

class Bone{
    public:
    float XPos, YPos, ZPos, XRot, YRot, ZRot, XSize, YSize, ZSize;
	dGeomID GeomID;
	dBodyID BodyID;
	int ShapeType;
    Bone(float, float, float, int);
	void SetRotation(float, float, float);
	void SetSize(float, float, float);
	void AddToSim(size_t);
	void Render();
	float DistanceFromOrigin();
	Bone* Copy();
	void RemoveFromSim();
};
#endif
