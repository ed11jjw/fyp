#include "Muscle.h"

Muscle::Muscle(float x, float y, float z, bool isBalance, float length, int boneIndex1, int boneIndex2){
	XPos = x;
	YPos = y;
	ZPos = z;
	CurrentLength = length;
	if(CurrentLength <=SizeLimit)
		CurrentLength = SizeLimit;
	PresetLength = CurrentLength;
	BoneIndex1 = boneIndex1;
	BoneIndex2 = boneIndex2;
	IsBalanceMuscle = isBalance;
	Pid = new PID();
}

void Muscle::SetRotation(float x, float y, float z){
	XRot = x;
    YRot = y;
    ZRot = z;
}

void Muscle::SetParams(float phase, float amplitude, float speed){
	Phase = phase;
	Amplitude = amplitude;
	Speed = speed;
	if(Amplitude > AmplitudeLimit)
		Amplitude = AmplitudeLimit;
	else if(Amplitude < 0)
		Amplitude = 0;
	if(Speed > SpeedLimit)
		Speed = SpeedLimit;
	else if(Speed < 0)
		Speed = 0;
	//Calculate the initial length if the phase is not 0
	float angle = Speed * Phase;
	if((sin(angle) * Amplitude) > CurrentLength - SizeLimit){
		float newAmp = (CurrentLength - SizeLimit) / sin(angle);
		Amplitude = newAmp;
	}
		
    InitialLength = CurrentLength - (sin(angle) * Amplitude);
	
}

void Muscle::AddToSim(size_t i, dBodyID boneID1, dBodyID boneID2){
	BoneID1 = boneID1;
	BoneID2 = boneID2;
	Pid = new PID();
	
	if(!IsBalanceMuscle){//If muscle isn't for balance, get the angle to calculate the 
		//current length, make sure length doesn't exceed constraint
		float angle = Speed * Phase;
		CurrentLength = InitialLength + (sin(angle) * Amplitude);
		if(CurrentLength < SizeLimit)
			CurrentLength = SizeLimit;
	}
	else{
		//Get intital angle between bones the muscle is connected to, set current length
		StartAngle = GetBonePairAngle();
		CurrentLength = PresetLength;
	}
	
    int j,k;
    dReal sides[3];
    dMass m;
	obj[i].body = dBodyCreate(world);
	BodyID = obj[i].body;
    dBodySetPosition(BodyID, XPos, YPos, ZPos);
	//Convert the rotation angles to radians
	float xRad = XRot * DEG_TO_RAD;
	float yRad = YRot * DEG_TO_RAD;
	float zRad = ZRot * DEG_TO_RAD;
	dMatrix3 R;
	ComposeXYZRotation(xRad, yRad, zRad, R);
	//Set the body rotation using the matrix
    dBodySetRotation(BodyID,R);
	//Create cylinder
	dMassSetCylinder(&m, DENSITY, 3, Muscle_Radius, CurrentLength);
	obj[i].geom[0] = dCreateCylinder(space, Muscle_Radius, CurrentLength);

	dGeomSetBody(obj[i].geom[0], BodyID);
	dBodySetMass(BodyID,&m);
	GeomID = obj[i].geom[0];
	MuscleGeoms.push_back(GeomID);
	
	//Create joints which anchor the muscle to it's bones
	dVector3 topEnd;
	dBodyGetRelPointPos (BodyID, 0, 0, CurrentLength/2, topEnd);
	dVector3 bottomEnd;
	dBodyGetRelPointPos (BodyID, 0, 0, -CurrentLength/2, bottomEnd);
	//Note, the top joint must be bone 1, bottom joint bone 2
	//May be good to automatically swap if the bones are given in the wrong order
	Joint1 = dJointCreateBall(world, jointgroup);
	dJointAttach(Joint1, BodyID, BoneID1);
	dJointSetBallAnchor(Joint1, topEnd[0], topEnd[1], topEnd[2]);
	
	Joint2 = dJointCreateBall(world, jointgroup);
	dJointAttach(Joint2, BodyID, BoneID2);
	dJointSetBallAnchor (Joint2, bottomEnd[0], bottomEnd[1], bottomEnd[2]);
}

void Muscle::AdjustLength(float worldTime){
	if(IsBalanceMuscle) //If muscle is used for balance, use balance set length function
		SetCurrentLengthBalance();
	else //Else use set length function that uses amplitude, phase and speed
		SetCurrentLength(worldTime);
	if(CurrentLength < SizeLimit) //Make sure length doesn't exceed constraint
		CurrentLength = SizeLimit;
	dGeomCylinderSetParams(GeomID, Muscle_Radius, CurrentLength);
	//Update the muscle's joints
	dVector3 topEnd;
	dBodyGetRelPointPos (BodyID, 0, 0, CurrentLength/2, topEnd);
	dJointSetDBallAnchor1(Joint1, topEnd[0], topEnd[1], topEnd[2]);
	dVector3 bottomEnd;
	dBodyGetRelPointPos (BodyID, 0, 0, -CurrentLength/2, bottomEnd);
	dJointSetDBallAnchor1(Joint2, bottomEnd[0], bottomEnd[1], bottomEnd[2]);
}

void Muscle::SetCurrentLength(float worldTime){
	//Get the angle of the muscle's cycle
	float angle = Speed * (worldTime + Phase);
	//Calculate the length from the point in the cycle
    CurrentLength = InitialLength + (sin(angle) * Amplitude);
}

void Muscle::SetCurrentLengthBalance() {
	//Get angle between two bones connecting the balance muscle
	//Current error is the difference between current angle and initial angle 
	float currentAngle = GetBonePairAngle();
	float error = currentAngle - StartAngle;
	
	//Update pid, add output velocity to current length 
	Pid->Update(error);
	CurrentLength += Pid->Output;
}

float Muscle::GetBonePairAngle(){
	//Gets the angle between the two bones that the balancing muscle connects
	const dReal* bone1Pos = dBodyGetPosition(BoneID1);
	dVector3 bone1Vector;
	dBodyGetRelPointPos(BoneID1, 0, 0, 1, bone1Vector);
	bone1Vector[0] -= bone1Pos[0];
	bone1Vector[1] -= bone1Pos[1];
	bone1Vector[2] -= bone1Pos[2];
	
	const dReal* bone2Pos = dBodyGetPosition(BoneID2);
	dVector3 bone2Vector;
	dBodyGetRelPointPos(BoneID2, 0, 0, 1, bone2Vector);
	bone2Vector[0] -= bone2Pos[0];
	bone2Vector[1] -= bone2Pos[1];
	bone2Vector[2] -= bone2Pos[2];
	
	return AngleBetweenVectors(bone1Vector, bone2Vector);
}

void Muscle::Render(){
    const dReal *pos = dGeomGetPosition(GeomID);
    const dReal *R = dGeomGetRotation(GeomID);
	dReal radius,length;
	//Render all muscles as cylinders
	dGeomCylinderGetParams(GeomID,&radius,&length);
	dsDrawCylinder(pos,R,length,radius);
}

Muscle* Muscle::Copy(){
	//Create next creature muscles in generation
	Muscle *m = new Muscle(XPos, YPos, ZPos, IsBalanceMuscle, PresetLength, BoneIndex1, BoneIndex2);
	m->SetRotation(XRot, YRot, ZRot);
	if(!IsBalanceMuscle)
		m->SetParams(Phase, Amplitude, Speed);
	return m;
}

void Muscle::RemoveFromSim(){
	//Remove muscle's body and contact points from simulation
	dBodyDestroy(BodyID);
	dGeomDestroy(GeomID);
}