#include "Joint.h"

Joint::Joint(float x, float y, float z, int boneIndex1, int boneIndex2, int type){
	//Set initial position and bone index
	XPos = x;
	YPos = y;
	ZPos = z;
	BoneIndex1 = boneIndex1;
	BoneIndex2 = boneIndex2;
	Type = type;
}

void Joint::AddToSim(dBodyID boneID1, dBodyID boneID2){
	BoneID1 = boneID1;
	BoneID2 = boneID2;
	if(Type == 0){ //Create hinge joint when type is 0.
		JointID = dJointCreateHinge(world, jointgroup);
		dJointAttach(JointID, BoneID1, BoneID2);
		dJointSetHingeAnchor(JointID, XPos, YPos, ZPos);
	}
	else if(Type == 1){ //Create universal joint when type is 1.
		JointID = dJointCreateUniversal(world, jointgroup);
		dJointAttach(JointID, BoneID1, BoneID2);
		dJointSetUniversalAnchor(JointID, XPos, YPos, ZPos);
	}
	else if(Type == 2){ //Create fixed joint when type is 2.
		JointID = dJointCreateFixed(world, jointgroup);
		dJointAttach(JointID, BoneID1, BoneID2);
		dJointSetFixed(JointID);
	}
	else { //Unrecognised type.
		printf("Error: joint type %d not recognised.\n", Type);
	}
}

Joint* Joint::Copy(){
	//Copy joint positions for next creature in generation.
	Joint *j = new Joint(XPos, YPos, ZPos, BoneIndex1, BoneIndex2, Type);
	return j;
}