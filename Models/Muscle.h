using namespace std;
#ifndef MUSCLE_H
#define MUSCLE_H
#include <iostream>
#include <vector>
#include <math.h>
#include <ode/ode.h>

#define Muscle_Radius 0.1 
#define SpeedLimit 2
#define AmplitudeLimit 2
#define SizeLimit 0.1

class Muscle{
    public:
    float XPos, YPos, ZPos, XRot, YRot, ZRot;
	float Phase, Amplitude, Speed, InitialLength, CurrentLength;
	float StartAngle, PresetLength;
	bool IsBalanceMuscle;
	int BoneIndex1, BoneIndex2;
	dGeomID GeomID;
	dBodyID BodyID, BoneID1, BoneID2;
	dJointID Joint1, Joint2;
    Muscle(float, float, float, bool, float, int, int);
	void SetRotation(float, float, float);
	void SetParams(float, float, float);
	void AdjustLength(float);
	void SetCurrentLength(float);
	void SetCurrentLengthBalance();
	void AddToSim(size_t, dBodyID, dBodyID);
	void Render();
	float GetBonePairAngle();
	Muscle* Copy();
	PID* Pid;
	void RemoveFromSim();
};
#endif
