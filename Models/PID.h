using namespace std;


class PID{
    public:
    float PreviousError, IntegralError, Output, Kp, Ki, Kd;
	
	PID();
	void Initialise();
	void Update(float);
};