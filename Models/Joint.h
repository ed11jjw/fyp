using namespace std;
#ifndef JOINT_H
#define JOINT_H
#include <iostream>
#include <vector>
#include <math.h>
#include <ode/ode.h>

class Joint{
    public:
    float XPos, YPos, ZPos;
	int BoneIndex1, BoneIndex2, Type;
	dBodyID BoneID1, BoneID2;
	dJointID JointID;
	Joint(float, float, float, int, int, int);
	void AddToSim(dBodyID boneID1, dBodyID boneID2);
	Joint* Copy();
};
#endif
