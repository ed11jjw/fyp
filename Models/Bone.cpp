#include "Bone.h"

Bone::Bone(float x, float y, float z, int type){
	//Set initial position
    XPos = x;
    YPos = y;
    ZPos = z;
	ShapeType = type;
}

void Bone::SetRotation(float x, float y, float z){
	//Set each rotation for each axis
	XRot = x;
    YRot = y;
    ZRot = z;
}

void Bone::SetSize(float x, float y, float z){
	//Set width, height and length
	XSize = x;
	YSize = y;
	ZSize = z;
}

void Bone::AddToSim(size_t i){
    int j,k;
    dReal sides[3];
    dMass m;
	obj[i].body = dBodyCreate(world);
	BodyID = obj[i].body;
    dBodySetPosition(BodyID, XPos, YPos, ZPos);
	//Convert the rotation angles to radians
	float xRad = XRot * DEG_TO_RAD;
	float yRad = YRot * DEG_TO_RAD;
	float zRad = ZRot * DEG_TO_RAD;
	dMatrix3 R;
	ComposeXYZRotation(xRad, yRad, zRad, R);
	//Set the body rotation using the matrix
    dBodySetRotation(BodyID,R);
	if (ShapeType == dBoxClass) { //Create box
		dMassSetBox(&m, DENSITY, XSize, YSize, ZSize);
		obj[i].geom[0] = dCreateBox(space, XSize, YSize, ZSize);
	
	} 
	else if (ShapeType == dCylinderClass) { //Create cylinder
		dMassSetCylinder(&m, DENSITY, 3, XSize * 0.5, YSize);
		obj[i].geom[0] = dCreateCylinder(space, XSize * 0.5, YSize);
	
	} 
	else if (ShapeType == dSphereClass) { //Create sphere
		dMassSetSphere (&m, DENSITY, XSize * 0.5);
		obj[i].geom[0] = dCreateSphere(space, XSize * 0.5);
	}
	else {
		printf("Error: shape type %d not recognised.\n", ShapeType);
	}
	dGeomSetBody(obj[i].geom[0], BodyID);

	dBodySetMass(BodyID,&m);
	GeomID = obj[i].geom[0];
}

void Bone::Render(){
    const dReal *pos = dGeomGetPosition(GeomID);
    const dReal *R = dGeomGetRotation(GeomID);
    if (ShapeType == dBoxClass) { // 1, draw box
        dVector3 sides;
        dGeomBoxGetLengths (GeomID,sides);
        dsDrawBox(pos,R,sides);
    } 
	else if (ShapeType == dSphereClass) { // 0, draw sphere
        dsDrawSphere(pos,R,dGeomSphereGetRadius(GeomID));
    } 
	else if (ShapeType == dCapsuleClass) { // 2, draw capsule
        dReal radius,length;
        dGeomCapsuleGetParams(GeomID,&radius,&length);
        dsDrawCapsule(pos,R,length,radius);
    } 
	else if (ShapeType == dCylinderClass) { //3, draw cylinder
        dReal radius,length;
        dGeomCylinderGetParams(GeomID,&radius,&length);
        dsDrawCylinder(pos,R,length,radius);
    }
}

float Bone::DistanceFromOrigin(){
	//Gets the bone's distance from the origin 
	//in x, y terms only
	const dReal *pos = dGeomGetPosition(GeomID);
	return Distance(pos[0], pos[1], 0);
}

Bone* Bone::Copy(){
	//Create next creature bones in generation
	Bone *b = new Bone(XPos, YPos, ZPos, ShapeType);
	b->SetRotation(XRot, YRot, ZRot);
	b->SetSize(XSize, YSize, ZSize);
	return b;
}

void Bone::RemoveFromSim(){
	//Destroy body and contact points
	dBodyDestroy(BodyID);
	dGeomDestroy(GeomID);
}
