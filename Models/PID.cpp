#include "PID.h"
#include <math.h>

PID::PID(){
	Initialise();
}

void PID::Initialise(){
	//Initialise PID values
	PreviousError = 0;
	IntegralError = 0;
	Kp = 0.001;
	Ki = 0.001;
	Kd = 0.001;
}

void PID::Update(float currentError){
	//Calculate integral error and differentiation from current error.
	float diffentiation;
	IntegralError += currentError*delta_t; 
	if(IntegralError > 1)
		IntegralError = 1;
	else if(IntegralError < -1)
		IntegralError = -1;
	diffentiation = ((currentError - PreviousError)/delta_t);
	PreviousError = currentError;
	
	//Get the velocity needed to try correct the error
	Output = (Kp * currentError) + (Ki * IntegralError) - (Kd * diffentiation);
}