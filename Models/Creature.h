using namespace std;
#ifndef CREATURE_H
#define CREATURE_H
#include <iostream>
#include <vector>
#include <array>
#include <math.h>
#include <ode/ode.h>

class Creature{
    public:
    vector<Bone*> Bones;
	vector<Muscle*> Muscles;
	vector<Joint*> Joints;
    float XPos, YPos, ZPos, InitialMaxHeight;
	float LastDistanceFromOrigin;
    Creature(string fileName, float, float, float);
	array<float, 3> eulerAngles(const float &q0, const float &q1, const float &q2, const float &q3);
	Creature(float, float, float);
    void AddBone(Bone *childBone);
	void AddMuscle(Muscle *childMuscle);
	void AddJoint(Joint *childJoint);
	void UpdateMuscles(float);
	size_t AddToSim(size_t);
	void Render();
	float DistanceFromOrigin();
	float MaxHeight();
	bool FallenOver();
	Creature* Copy();
	void RemoveFromSim();
};
#endif
