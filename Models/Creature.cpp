#include "Bone.cpp"
#include "Muscle.cpp"
#include "Joint.cpp"
#include "Creature.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>  
#include <sstream>
#include <fstream>  

Creature::Creature(string fileName, float x, float y, float z){
	XPos = x;
	YPos = y;
	ZPos = z;
	ifstream file(fileName.c_str());
	string line;
    getline(file,line); //Skip comment line
	getline(file,line);
	int numBones = atoi(line.c_str());
	getline(file,line); //Skip comment line
	string cell;//get csv cell
	if(numBones == 0)
		printf("No bones found in creature file. Is there a problem with the file?\n");
	for(int i = 0; i<(int)numBones; i++){
		getline(file,line); //Get position line
		stringstream  lineStream1(line);//
		getline(lineStream1, cell, ',');
		float xp = atof(cell.c_str()) + x; //X pos
		getline(lineStream1, cell, ',');
		float yp = atof(cell.c_str()) + y; //Y pos
		getline(lineStream1, cell, ',');
		float zp = atof(cell.c_str()) + z; //Z pos
		getline(lineStream1, cell, ',');
		int type = atoi(cell.c_str()); //Shape type
		AddBone(new Bone(xp, yp, zp, type));
		
		getline(file,line); //Get size coordinates
		stringstream  lineStream2(line);//
		getline(lineStream2, cell, ',');
		float xs = atof(cell.c_str()); //X size
		getline(lineStream2, cell, ',');
		float ys = atof(cell.c_str()); //Y size
		getline(lineStream2, cell, ',');
		float zs = atof(cell.c_str()); //Z size
		Bones[i]->SetSize(xs, ys, zs);
		
		if(type > 0){ //Rotation not needed for spheres
			getline(file,line); //Get rotation coordinates
			stringstream  lineStream3(line);//
			getline(lineStream3, cell, ',');
			float xr = atof(cell.c_str()); //X size
			getline(lineStream3, cell, ',');
			float yr = atof(cell.c_str()); //Y size
			getline(lineStream3, cell, ',');
			float zr = atof(cell.c_str()); //Z size
			Bones[i]->SetRotation(xr, yr, zr);
		}
		else {
			//Set rotation to 0 for spheres
			Bones[i]->SetRotation(0, 0, 0);
		}
			
	}
	
	
	getline(file,line); //Skip comment line
	getline(file,line);
	int numJoints = atoi(line.c_str());
	getline(file,line); //Skip comment line
	for(int i = 0; i<(int)numJoints; i++){
		getline(file,line); //Get position line
		stringstream  lineStream4(line);//
		getline(lineStream4, cell, ',');
		float xp = atof(cell.c_str()) + x; //X pos
		getline(lineStream4, cell, ',');
		float yp = atof(cell.c_str()) + y; //Y pos
		getline(lineStream4, cell, ',');
		float zp = atof(cell.c_str()) + z; //Z pos
		getline(lineStream4, cell, ',');
		int b1 = atoi(cell.c_str()); //Body 1
		getline(lineStream4, cell, ',');
		int b2 = atoi(cell.c_str()); //Body 2
		getline(lineStream4, cell, ',');
		int type = atoi(cell.c_str()); //Shape type
		AddJoint(new Joint(xp, yp, zp, b1, b2, type));
	}
	
	getline(file,line); //Skip comment line
	getline(file,line);
	int numMuscles = atoi(line.c_str());
	getline(file,line); //Skip comment line
	for(int i = 0; i < numMuscles; i++){
		getline(file,line); //Get position line
		stringstream  lineStream5(line);//
		getline(lineStream5, cell, ',');
		float xp = atof(cell.c_str()) + x; //X pos
		getline(lineStream5, cell, ',');
		float yp = atof(cell.c_str()) + y; //Y pos
		getline(lineStream5, cell, ',');
		float zp = atof(cell.c_str()) + z; //Z pos
		getline(lineStream5, cell, ',');
		int balanceInt = atoi(cell.c_str()); //Is balance muscle? 
		bool balance = false; 
		if(balanceInt == 1) // 1 = true, 0 = false
			balance = true;
		
		getline(file,line);
		stringstream  lineStream6(line);//
		getline(lineStream6, cell, ',');
		float len = atof(cell.c_str()); //Length
		getline(lineStream6, cell, ',');
		int b1 = atoi(cell.c_str()); //Body 1
		getline(lineStream6, cell, ',');
		int b2 = atoi(cell.c_str()); //Body 2
		AddMuscle(new Muscle(xp, yp, zp, balance, len, b1, b2));
		
		getline(file,line);
		stringstream  lineStream7(line);//
		getline(lineStream7, cell, ',');
		float xr = atof(cell.c_str()); //X rotation
		getline(lineStream7, cell, ',');
		float yr = atof(cell.c_str()); //Y rotation
		getline(lineStream7, cell, ',');
		float zr = atof(cell.c_str()); //Z rotation
		Muscles[i]->SetRotation(xr, yr, zr);
		
		getline(file,line);
		//Phase, amp, speed not relevant for balance muscles
		if(!balance){
			stringstream  lineStream8(line);//
			getline(lineStream8, cell, ',');
			float p = atof(cell.c_str()); //Phase
			getline(lineStream8, cell, ',');
			float a = atof(cell.c_str()); //Amplitude
			getline(lineStream8, cell, ',');
			float s = atof(cell.c_str()); //Speed
			Muscles[i]->SetParams(p, a, s);
		}
	}
}

Creature::Creature(float x, float y, float z){
	//Set initial position
	XPos = x;
	YPos = y;
	ZPos = z;
}

void Creature::AddBone(Bone *childBone){
	//Add bone to bones array
	Bones.push_back(childBone);
}

void Creature::AddMuscle(Muscle *childMuscle){
	//Add muscle to muscles array
	Muscles.push_back(childMuscle);
}

void Creature::AddJoint(Joint *childJoint){
	//Add joint to joints array
	Joints.push_back(childJoint);
}

size_t Creature::AddToSim(size_t index){
	//Stop ODE non-determistic results
	dRandSetSeed(42);
	for (int i=0; i<Bones.size(); i++) {//Add each bone to simulation
		Bones[i]->AddToSim(index);
		index++;
    }
	for (int i=0; i<Joints.size(); i++) {//Add each joint to simulation
		dBodyID b1 = Bones[Joints[i]->BoneIndex1]->BodyID;
		dBodyID b2 = Bones[Joints[i]->BoneIndex2]->BodyID;
		Joints[i]->AddToSim(b1, b2);
    }
	for (int i=0; i<Muscles.size(); i++) {//Add each muscle to simulation
		dBodyID b1 = Bones[Muscles[i]->BoneIndex1]->BodyID;
		dBodyID b2 = Bones[Muscles[i]->BoneIndex2]->BodyID;
		Muscles[i]->AddToSim(index, b1, b2);
		index++;
	}
	InitialMaxHeight = MaxHeight(); //Set initial height
	return index;
}

void Creature::Render(){
	dsSetColor(1,1,0);//yellow
	for (int i=0; i<Bones.size(); i++) //Draw each bone
		Bones[i]->Render();
	dsSetColor(1,0,0);//red
    for (int i=0; i<Muscles.size(); i++) //Draw each muscle
		Muscles[i]->Render();
}

void Creature::UpdateMuscles(float world_timestep){
    for (int i=0; i<Muscles.size(); i++) //Adjust length for each muscle
		Muscles[i]->AdjustLength(world_timestep);
}

float Creature::DistanceFromOrigin(){
	//Gets the creature travel distance
	//By averaging it's bones distances from the origin
	float sum = 0;
	for (int i=0; i<Bones.size(); i++)
		sum += Bones[i]->DistanceFromOrigin();
	LastDistanceFromOrigin = sum / Bones.size();
	return LastDistanceFromOrigin;
}

float Creature::MaxHeight(){
	//Gets the maximum vertical length of the creature
	float max = 0;
	for (int i=0; i<Bones.size(); i++) {
		const dReal *pos = dGeomGetPosition(Bones[i]->GeomID);
		if(pos[2] > max)
			max = pos[2];
	}
	return max;
}

bool Creature::FallenOver(){
	//If current height is less than 3/4ths creatures initial height
	//Creature has fallen over, if height is heigher than 5/4ths it'sb_type
	//Initial height, creature is 'flying', i.e moving unrealistically
	float h = MaxHeight();
	return h < 0.75 * InitialMaxHeight || h > 1.25 * InitialMaxHeight;
}

Creature* Creature::Copy(){
	//Copy creatures position, bones, muscles and joints.
	Creature *c = new Creature(XPos, YPos, ZPos);
	for (int i=0; i<Bones.size(); i++)
		c->AddBone(Bones[i]->Copy());
	for (int i=0; i<Joints.size(); i++)
		c->AddJoint(Joints[i]->Copy());
	for (int i=0; i<Muscles.size(); i++)
		c->AddMuscle(Muscles[i]->Copy());
	return c;
}

void Creature::RemoveFromSim(){
	//Destroy bone and muscle, geoms and bodys from simulation
	dJointGroupEmpty(jointgroup);
	MuscleGeoms.clear();
	for (int i=0; i<Bones.size(); i++)
		Bones[i]->RemoveFromSim();
	for (int i=0; i<Muscles.size(); i++)
		Muscles[i]->RemoveFromSim();
}