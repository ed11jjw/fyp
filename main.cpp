using namespace std;
#include <vector>
#include <unistd.h>
#include <ode/ode.h>
#include "OdeHelpers/constants.h"
#include "OdeHelpers/mathHelpers.cpp"
#include "GlobalVariables.h"
#include <drawstuff/drawstuff.h>
#include <drawstuff/resource.h>
#include "OdeHelpers/texturepath.h"
#include "OdeHelpers/callbacks.cpp"
#include "OdeHelpers/stringFunctions.cpp"
#include "Models/Creature.cpp"
#include "GeneticAlgorithm/GeneticAlgorithm.cpp"

Creature *creature;
GeneticAlgorithm *genAlg;
static float view_xyz[3] = {4.0f,-30.0f,5.0f};
static float view_hpr[3] = {90.0f,5.0f,0.0f};
static bool paused = true;
static float camera_speed = 0.3;
static float world_timestep = 0;
static int frame = 0;
static size_t bodyIndex = 0; 
static bool evolve = false; //Run genetic algorithm or just simulate the creature?
static bool display = true;
static dsFunctions fn; //Holds pointers to drawstuff callback functions
dThreadingImplementationID threading; //Holds ODE threading info
dThreadingThreadPoolID pool; //Holds ODE threading info

static void nearCallback (void *, dGeomID o1, dGeomID o2)
{
	nearCallbackDelegate(o1, o2);
}

// start simulation - set viewpoint
static void start()
{
    dAllocateODEDataForThread(dAllocateMaskAll);
    dsSetViewpoint (view_xyz, view_hpr);
    printf ("Begin simulation\n");
}

// called when a key pressed
static void command(int cmd)
{
    cmd = locase(cmd);
	if (cmd == 'w') {
		view_xyz[1]+= camera_speed;//forwards
	}
	else if (cmd == 's') {
		view_xyz[1]-= camera_speed;//backwards
	}
	if (cmd == 'a') {
		view_xyz[0]-= camera_speed;//left
	}
	else if (cmd == 'd') {
		view_xyz[0]+= camera_speed;//right
	}
	if (cmd == 'r') {
		view_xyz[2]+= camera_speed;//up
	}
	else if (cmd == 'f') {
		view_xyz[2]-= camera_speed;//down
	}
	else if(cmd == 'p')
	{
		paused = !paused;//pause or unpause
	}
	
	if (cmd == 'w' || cmd == 's' || cmd == 'a' || cmd == 'd' || cmd == 'r' || cmd == 'f') {
		dsGetViewpoint(NULL,view_hpr);
		dsSetViewpoint(view_xyz, view_hpr);//update view point
	}

}

// simulation loop
static void simLoop(int pause)
{
    if (!paused){
		//Run collision detection test
        dSpaceCollide(space, 0, &nearCallback);
        //dWorldQuickStep(world, 0.02);
		dWorldStep(world, delta_t);
		// remove all contact joints
		dJointGroupEmpty(contactgroup);
		world_timestep+= 0.01;
		frame++;
		bool fallenOver = creature->FallenOver();
		//if creature hasn't fallen over update muscle location
		if(!creature->FallenOver())
			creature->UpdateMuscles(world_timestep);
		creature->DistanceFromOrigin();
		//if creature has fallen over when evolve command has been invoked generate next creature.
		if(evolve && (fallenOver || frame > 100000))
		{
			creature = genAlg->NextCreature(creature);
			world_timestep = 0;
			frame = 0;
		}
		
	}
	if(display){
		dsSetTexture(DS_WOOD);
		creature->Render();
	}
}

void InitialiseODE()
{
	// setup pointers to drawstuff callback functions
    fn.version = DS_VERSION;
    fn.start = &start;
    fn.step = &simLoop;
    fn.command = &command;
    fn.stop = 0;
    fn.path_to_textures = DRAWSTUFF_TEXTURE_PATH;

    // create world
    dInitODE2(0);
    world = dWorldCreate();
    space = dHashSpaceCreate(0);
    contactgroup = dJointGroupCreate(0);
	jointgroup = dJointGroupCreate(0);
    dWorldSetGravity(world,0,0,-GRAVITY);
    dWorldSetCFM(world,1e-3);
	dWorldSetERP(world, 0.5);
    dWorldSetAutoDisableFlag(world,0);
    dWorldSetLinearDamping(world, 0.00001);
    dWorldSetAngularDamping(world, 0.005);
    dWorldSetMaxAngularSpeed(world, 10);
    dWorldSetContactMaxCorrectingVel(world,100);
    dWorldSetContactSurfaceLayer(world,0.001);
    dCreatePlane(space,0,0,1,0);
    memset(obj,0,sizeof(obj));

	if(display){
		threading = dThreadingAllocateMultiThreadedImplementation();
		dThreadingThreadPoolID pool = dThreadingAllocateThreadPool(4, 0, dAllocateFlagBasicData, NULL);
		dThreadingThreadPoolServeMultiThreadedImplementation(pool, threading);
		// dWorldSetStepIslandsProcessingMaxThreadCount(world, 1);
		dWorldSetStepThreadingImplementation(world, dThreadingImplementationGetFunctions(threading), threading);
	}
}

void CloseDownODE()
{
	if(display){
		dThreadingImplementationShutdownProcessing(threading);
		dThreadingFreeThreadPool(pool);
		dWorldSetStepThreadingImplementation(world, NULL, NULL);
		dThreadingFreeImplementation(threading);
	}

    dJointGroupDestroy(contactgroup);
    dSpaceDestroy(space);
    dWorldDestroy(world);
    dCloseODE();
}

int main (int argc, char **argv)
{
	if (argc < 2)
	{	//If there is only the main argument command, no creature file is specified.
		printf ("No creature file argument specified.\n");
		return 1;
	}
	
	//If argument is greater than two and the third argument is evolve, create log and set
	//boolean of evolve equal to true.
	if (argc > 2 && std::string(argv[2]) == "evolve")
	{
		evolve = true;
		//Initialise log file
		ofstream logFile;
		logFile.open ("Log.csv");
		logFile << "Generation, Average dist, Best dist, Worst dist\n";
		logFile.close();
		printf("Running genetic algorithm.\n");
	}
	
	if (argc > 3 && std::string(argv[3]) == "nodisplay")
	{
		display = false;
		paused = false;
		printf("Running without display.\n");
	}
	
	InitialiseODE();
	
	std::string arg1(argv[1]);
	arg1 = "Creatures\\" + arg1;
	//Check that the file exists
	if(access(arg1.c_str(), F_OK ) == -1 )
	{
		printf ("Creature file %s does not exist.\n", arg1.c_str());
		return 1;
	}
	//Load the creature
	creature = new Creature(arg1,0,0,0.1);
    bodyIndex = creature->AddToSim(0);
	genAlg = new GeneticAlgorithm(creature, display);
	
    // run simulation
	if(display){
		dsSimulationLoop(argc,argv,640,480,&fn);
	}
	else{
		while(genAlg->CurrentGeneration < 250)
			simLoop(0);
	}
	
	CloseDownODE();
}


