#include <math.h>
#include <ode/ode.h>

float Distance(float x, float y, float z)
{
	float distSq = x * x + y * y + z * z;
	return sqrt(distSq);
}

float Distance(float x1, float y1, float z1, float x2, float y2, float z2)
{
	float xDiff = x1 - x2;
	float yDiff = y1 - y2;
	float zDiff = z1 - z2;
	return Distance(xDiff, yDiff, zDiff);
}

void ComposeXYZRotation(float x, float y, float z, dMatrix3 result)
{
    //Get quaternions for x, y, z angles
	dQuaternion xq;
	dQFromAxisAndAngle (xq, 1, 0, 0, x);
	dQuaternion yq;
	dQFromAxisAndAngle (yq, 0, 1, 0, y);
	dQuaternion zq;
	dQFromAxisAndAngle (zq, 0, 0, 1, z);
	//Combine x quarternion with y quarternion
	dQuaternion xyq;
	dQMultiply0 (xyq, yq, xq);
	//Combine xy quarternion with z quarternion
	dQuaternion xyzq;
	dQMultiply0 (xyzq, zq, xyq);
	//Get rotation matrix from quarternion
	dQtoR (xyzq, result);
}

float AngleBetweenVectors(dVector3 v1, dVector3 v2)
{
	float dotProd = 
		v1[0] * v2[0] +
		v1[1] * v2[1] +
		v1[2] * v2[2];
		
	float len1 = Distance(v1[0], v1[1], v1[2]);
	float len2 = Distance(v2[0], v2[1], v2[2]);
	float cosTheta = dotProd / (len1 * len2);
	return acos(cosTheta);
}







