// this is called by dSpaceCollide when two objects in space are
// potentially colliding.

static int show_contacts = 0;	// show contact points?
static int doFeedback=0;
static int fbnum=0;
struct MyFeedback {
  dJointFeedback fb;
  bool first;
};
static MyFeedback feedbacks[MAX_FEEDBACKNUM];

static void nearCallbackDelegate (dGeomID o1, dGeomID o2)
{
    int i;

	//Ignore collision detection for muscles
	for(i = 0; i<MuscleGeoms.size(); i++){
		if(MuscleGeoms[i] == o1 || MuscleGeoms[i] == o2)
			return;
	}
	
    // exit without doing anything if the two bodies are connected by a joint
    dBodyID b1 = dGeomGetBody(o1);
    dBodyID b2 = dGeomGetBody(o2);

    if (b1 && b2 && dAreConnectedExcluding(b1,b2,dJointTypeContact))
        return;
	
    dContact contact[MAX_CONTACTS];
	int n = dCollide(o1, o2, MAX_CONTACTS, &contact[0].geom, sizeof(dContact));
	if (n > 0) {
		for (i=0; i<n; i++) {
			contact[i].surface.mode = dContactSoftERP  | dContactApprox1;
			contact[i].surface.mu = dInfinity;
			contact[i].surface.soft_erp = 0.5;
			dJointID c = dJointCreateContact (world,contactgroup,&contact[i]);
			dJointAttach (c,
				dGeomGetBody(contact[i].geom.g1),
				dGeomGetBody(contact[i].geom.g2));
		}
    }
}