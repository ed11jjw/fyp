Final year project

Instructions to make in windows:

Download ODE

Download MingW compiler

Open an MSYS terminal and cd to the ODE folder

Run the following commands to make and install ODE:


libtoolize

autoreconf

automake --add-missing

./configure

make

make-install


Then to use the drawstuff libaries:


cd to \ode\drawstuff\src

windres resources.rc -o resources.o

copy resources.o to the fyp folder


To build the main.cpp:

g++ main.cpp -o main.exe -lode -ldrawstuff -lopengl32 -lglu32 -lgdi32 -lwinmm resources.o -std=c++0x

or on windows just run "Compile windows.bat"


To run:

'main.exe creature.csv' -to simulate creature only

'main.exe creature.csv evolve' -to simulate creature and run genetic algorithm

'main.exe creature.csv evolve nodisplay' -to simulate creature and run genetic 
algorithm with no display

Keyboard commands to move around simulation:

'p' - pause/unpause simulation

'w' - foward

's' - backwards

'a' - sidewards (left)

'd' - sidewards (right)

'r' - upwards

'f' - downwards

Project report can be accessed here: https://minerva.leeds.ac.uk/bbcswebdav/orgs/SCH_Computing/FYProj/reports/1516/JWILSON.pdf